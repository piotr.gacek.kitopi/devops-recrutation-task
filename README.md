# devops recrutation task


## prerequisites
```
1. Copy the application code with requirements file into your code repository
 and share it with us.
```

## task requirements
```
1. Dockerize provided python app. Create the Dockerfile with every good practice you know about.
2. Create the CICD pipeline file on gitlab or any tool you know/want to know, to automate process of building and pushing image into any public docker repository.
3. Create a helm chart to deploy this application on a Kubernetes cluster - deployment + service + ingress.
4. Create argocd application manifest to run this app on the argocd cluster.
5. Add Dockerfile, CICD file, helm chart code, generated app manifests and argocd manifest into the repository.
```


In case of any issues or questions, you can reach one of us via email.

`sebastian@kitopi.com`

`tomasz.nowodzinski@kitopi.com`

`piotr.gacek@kitopi.com`

Also, please add `marcelina.wierzbicka@kitopi.com` in CC.
